import 'package:dio/dio.dart';
import 'package:full_going_app/config/config_api.dart';
import 'package:full_going_app/functions/token_lib.dart';
import 'package:full_going_app/model/my_history_detail_item_result.dart';
import 'package:full_going_app/model/my_history_list_result.dart';
import 'package:full_going_app/model/using_end_kick_board_Item_result.dart';
import 'package:full_going_app/model/using_kick_board_item_result.dart';

class RepoKickboardHistory {
  // 다트문법. 페이지 언급안하면 기본값 1 주고싶을때.. {} 여기 안에 이렇게 쓰면 됨.
  // 자바에선 안됨.. 다트가 더 늦게 나온 언어라서 기존 언어 단점이 보완된거임..불편하니까..
  Future<MyHistoryListResult> getList({int page = 1}) async {
    final String _baseUrl = '$apiUri/kick-board-history/my?page={page}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{page}', page.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MyHistoryListResult.fromJson(response.data);

  }


  Future<MyHistoryDetailItemResult> getDetailHistory(int historyId) async {
    final String _baseUrl = '$apiUri/kick-board-history/my/history-id/{historyId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{historyId}', historyId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return MyHistoryDetailItemResult.fromJson(response.data);

  }

  Future<UsingKickBoardItemResult> getUsingKickBoard() async {
    final String _baseUrl = '$apiUri/kick-board-history/my/using/history';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return UsingKickBoardItemResult.fromJson(response.data);

  }

  Future<UsingEndKickBoardItemResult> getUsingEndKickBoard(int historyId) async {
    final String _baseUrl = '$apiUri/kick-board-history/my/history-id/{historyId}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.get(_baseUrl.replaceAll('{historyId}', historyId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return UsingEndKickBoardItemResult.fromJson(response.data);

  }

}