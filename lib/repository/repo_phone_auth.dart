import 'package:dio/dio.dart';
import 'package:full_going_app/config/config_api.dart';
import 'package:full_going_app/model/auth_check_request.dart';
import 'package:full_going_app/model/auth_send_request.dart';
import 'package:full_going_app/model/common_result.dart';

class RepoPhoneAuth {
  Future<CommonResult> doSend(AuthSendRequest request) async {
    const String baseUrl = '$apiUri/auth/message/send';

    Dio dio = Dio();

    final response = await dio.post(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false
        )
    );

    return CommonResult.fromJson(response.data);
  }

  Future<CommonResult> doCheck(AuthCheckRequest request) async {
    const String baseUrl = '$apiUri/auth/number/check';

    Dio dio = Dio();

    final response = await dio.put(
        baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false
        )
    );

    return CommonResult.fromJson(response.data);
  }
}