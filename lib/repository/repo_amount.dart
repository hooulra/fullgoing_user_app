import 'package:dio/dio.dart';
import 'package:full_going_app/config/config_api.dart';
import 'package:full_going_app/functions/token_lib.dart';
import 'package:full_going_app/model/common_result.dart';
import 'package:full_going_app/model/remain_price_request.dart';

class RepoAmount {
  Future<CommonResult> doCharge(RemainPriceRequest remainPriceRequest) async {
    const String baseUrl = '$apiUri/remaining-amount/my/plus/price';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';

    final response = await dio.put(
        baseUrl,
        data: remainPriceRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }


  Future<CommonResult> doChargeFullGoingPass({required String hourStr}) async {
    const String baseUrl = '$apiUri/remaining-amount/my/plus/pass?pass-type={hourStr}';

    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ${token!}';
    final response = await dio.put(
        baseUrl.replaceAll('{hourStr}',
            hourStr
        ),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return CommonResult.fromJson(response.data);
  }
}