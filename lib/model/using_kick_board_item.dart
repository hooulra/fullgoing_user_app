class UsingKickBoardItem {
  String dateStart;
  int historyId;
  String kickBoardModelName;
  num priceBase;
  num priceMinute;
  String priceName;
  num startPosX;
  num startPosY;

  UsingKickBoardItem(
    this.dateStart,
    this.historyId,
    this.kickBoardModelName,
    this.priceBase,
    this.priceMinute,
    this.priceName,
    this.startPosX,
    this.startPosY,
  );

  factory UsingKickBoardItem.fromJson(Map<String, dynamic> json) {
    return UsingKickBoardItem(
      json['dateStart'],
      json['historyId'],
      json['kickBoardModelName'],
      json['priceBase'],
      json['priceMinute'],
      json['priceName'],
      json['startPosX'],
      json['startPosY'],
    );
  }
}
