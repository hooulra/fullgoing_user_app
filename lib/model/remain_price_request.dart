class RemainPriceRequest {
  double price;

  RemainPriceRequest(this.price);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['price'] = this.price;

    return data;
  }
}
