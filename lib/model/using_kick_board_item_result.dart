import 'package:full_going_app/model/using_kick_board_item.dart';

class UsingKickBoardItemResult {
  int code;
  bool isSuccess;
  String msg;
  UsingKickBoardItem data;

  UsingKickBoardItemResult(this.code, this.isSuccess, this.msg, this.data);

  factory UsingKickBoardItemResult.fromJson(Map<String, dynamic> json) {
    return UsingKickBoardItemResult(
      json['code'],
      json['isSuccess'],
      json['msg'],
      UsingKickBoardItem.fromJson(json['data']),
    );
  }
}