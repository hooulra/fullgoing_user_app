import 'package:full_going_app/model/login_response.dart';

class LoginResult {
  LoginResponse data;
  bool isSuccess; // 성공 여부를 묻는 변수
  int code; // isSuccess 결과에 따른 코드 값을 담는 변수
  String msg; // isSuccess 결과에 따른 메세지를 담는 변수

  LoginResult(this.data, this.isSuccess, this.code, this.msg);

  factory LoginResult.fromJson(Map<String, dynamic> json) {
    return LoginResult(
        LoginResponse.fromJson(json['data']),
        json['isSuccess'] as bool,
        json['code'],
        json['msg']
    );
  }
}
