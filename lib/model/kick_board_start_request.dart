class KickBoardStartRequest {
  double startPosX;
  double startPosY;

  KickBoardStartRequest(this.startPosX, this.startPosY);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['startPosX'] = this.startPosX;
    data['startPosY'] = this.startPosY;

    return data;
  }
}