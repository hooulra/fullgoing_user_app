class MemberPasswordFindRequest {
  String username;
  String phoneNumber;

  MemberPasswordFindRequest(this.username, this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['username'] = this.username;
    data['phoneNumber'] = this.phoneNumber;

    return data;
  }
}