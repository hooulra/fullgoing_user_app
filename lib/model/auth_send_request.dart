class AuthSendRequest {
  String phoneNumber;

  AuthSendRequest(this.phoneNumber);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['phoneNumber'] = this.phoneNumber;

    return data;
  }
}