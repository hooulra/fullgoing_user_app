class PasswordChangeRequest {
  String currentPassword;
  String newPassword;
  String newRePassword;


  PasswordChangeRequest(this.currentPassword, this.newPassword, this.newRePassword);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();

    data['currentPassword'] = this.currentPassword;
    data['newPassword'] = this.newPassword;
    data['newRePassword'] = this.newRePassword;

    return data;
  }
}