import 'package:flutter/material.dart';
import 'package:full_going_app/config/config_size.dart';

class ComponentSectionTitle extends StatelessWidget {
  final String title;
  const ComponentSectionTitle(this.title, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(20),
      child: Text(
          title,
          style: const TextStyle(
              color: Colors.black,
              fontSize: fontSizeBig,
              fontWeight: FontWeight.w500,
              height: 1.3
          )
      ),
    );
  }
}
