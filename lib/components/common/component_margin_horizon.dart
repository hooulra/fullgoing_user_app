import 'package:flutter/material.dart';
import 'package:full_going_app/enums/enum_size.dart';

class ComponentMarginHorizon extends StatelessWidget {
  final EnumSize enumSize;

  const ComponentMarginHorizon({Key? key, this.enumSize = EnumSize.micro}) : super(key: key);

  double _getSize() {
    switch(enumSize) {
      case EnumSize.micro:
        return 3;
      case EnumSize.small:
        return 5;
      case EnumSize.mid:
        return 10;
      case EnumSize.big:
        return 15;
      case EnumSize.bigger:
        return 20;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _getSize(),
    );
  }
}

