import 'package:flutter/material.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_path.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';

class ComponentAppbarLogo extends StatelessWidget implements PreferredSizeWidget {
  final String imgLogo;
  final double logoHeight;
  final bool useIcon;
  final IconData actionIcon;
  final VoidCallback callback;

  const ComponentAppbarLogo(
      this.imgLogo,
      this.callback,
      {Key? key, this.logoHeight = sizeAppBarLogoHeight, this.useIcon = false, this.actionIcon = Icons.refresh}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: false,
      iconTheme: const IconThemeData(
        color: Colors.black,
      ),
      backgroundColor: colorPrimary,
      title: Image.asset(
        pathBase + imgLogo,
        fit: BoxFit.contain,
        height: logoHeight,
      ),
      elevation: appBarElevation,
      actions: [
        useIcon ? IconButton(
          onPressed: callback,
          icon: Icon(actionIcon),
        ) : const Center()
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(sizeAppBarHeight);
}
