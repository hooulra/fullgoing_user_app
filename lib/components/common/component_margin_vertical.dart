import 'package:flutter/material.dart';
import 'package:full_going_app/enums/enum_size.dart';

class ComponentMarginVertical extends StatelessWidget {
  final EnumSize enumSize;

  const ComponentMarginVertical({Key? key, this.enumSize = EnumSize.small}) : super(key: key);

  double _getSize() {
    switch(enumSize) {
      case EnumSize.micro:
        return 5;
      case EnumSize.small:
        return 10;
      case EnumSize.mid:
        return 20;
      case EnumSize.big:
        return 30;
      case EnumSize.bigger:
        return 50;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: _getSize(),
    );
  }
}
