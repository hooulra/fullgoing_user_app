import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/model/my_history_detail_item_result.dart';
import 'package:full_going_app/repository/repo_kickboard_history.dart';

class PageMyUseHistoryDetail extends StatefulWidget {
  const PageMyUseHistoryDetail({super.key, required this.historyId});

  final int historyId;

  @override
  State<PageMyUseHistoryDetail> createState() => _PageMyUseHistoryDetailState();
}

class _PageMyUseHistoryDetailState extends State<PageMyUseHistoryDetail> {
  String dateEnd = "";
  String dateStart = "";
  String kickBoardName = "";
  num resultPass = 0;
  num resultPrice = 0;

  Future<void> _loadProfileDetailData() async {
    MyHistoryDetailItemResult result =
        await RepoKickboardHistory().getDetailHistory(widget.historyId);

    setState(() {
      dateEnd = result.data.dateEnd;
      dateStart = result.data.dateStart;
      kickBoardName = result.data.kickBoardName;
      resultPass = result.data.resultPass;
      resultPrice = result.data.resultPrice;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadProfileDetailData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '킥보드 이용 상세내역',
      ),
      body: _buildBody(),
    );
  }

  Widget _buildInfoRow(String title, String value, {bool isHighlight = false}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(title,
            style: TextStyle(
                fontSize: 16,
                color: isHighlight ? colorPrimary : colorDarkGray)),
        Text(value,
            style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold)),
      ],
    );
  }

  Widget _buildBody() {
    return ListView(
      children: [
        Container(
          padding: const EdgeInsets.only(top: 50,bottom: 20, right: 20, left: 20),
          margin: const EdgeInsets.only(bottom: 10),
          child: Container(
            padding: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: colorLightGray),
              borderRadius: BorderRadius.circular(buttonRadius),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    "이용 내역",
                    style: TextStyle(
                      fontSize: fontSizeBig,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
                const ComponentMarginVertical(),
                const Divider(height: 2),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "킥보드 모델명",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      kickBoardName,
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "킥보드 사용 시작 시간",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      dateStart,
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "킥보드 사용 종료 시간",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      dateEnd,
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "결제 금액",
                      style: TextStyle(
                          fontSize: fontSizeBig, fontWeight: FontWeight.w700),
                    ),
                    Text(
                      maskWonFormatter.format(resultPrice) + " 원",
                      style: const TextStyle(
                          fontSize: fontSizeBig, fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
                const ComponentMarginVertical(),
                const Divider(height: 2),
                const ComponentMarginVertical(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "사용 패스 시간",
                      style: TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                    Text(
                      "${resultPass} 분",
                      style: const TextStyle(
                        fontSize: fontSizeMid,
                      ),
                    ),
                  ],
                ),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: ComponentTextBtn(
                    iconData: Icons.check_circle,
                    text: "확인",
                    textColor: Colors.white,
                    borderColor: colorPrimary,
                    bgColor: colorPrimary,
                    callback: () {
                      Navigator.pop(context);
                    },
                  ),
                ),

              ],
            ),
          ),
        ),
      ],
    );
  }




}
