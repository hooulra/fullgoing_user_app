import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_count_title.dart';
import 'package:full_going_app/components/common/component_custom_loading.dart';
import 'package:full_going_app/components/common/component_no_contents.dart';
import 'package:full_going_app/components/component_list_textline_item.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/model/my_charge_item.dart';
import 'package:full_going_app/repository/repo_charge_history.dart';


class PageMyChargeHistory extends StatefulWidget {
  const PageMyChargeHistory({super.key});

  @override
  State<PageMyChargeHistory> createState() => _PageMyChargeHistoryState();
}

class _PageMyChargeHistoryState extends State<PageMyChargeHistory> {
  /*
  * 인피니티 스크롤 구현할거임.
  * 그래서 스크롤을 컨트롤해야함..
  * 모바일에서는 페이징할때 버튼으로 하면 힘드니까
  * 페이지 바닥 찍으면 다음페이지 더 추가해서 보고..
  * 이런식으로 해야함..
  * 구글에 인피니티 스크롤 검색해보기.
  * */
  final _scrollController = ScrollController();

  List<MyChargeItem> _list = [];
  int _totalItemCount = 0;
  int _totalPage = 1;
  int _currentPage = 1;

  Future<void> _loadItems({bool reFresh = false}) async {
    // 새로고침하면 초기화시키기.
    if (reFresh) {
      _list = [];
      _totalItemCount = 0;
      _totalPage = 1;
      _currentPage = 1;
    }

    if (_currentPage <= _totalPage) {
      BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
        return ComponentCustomLoading(
          cancelFunc: cancelFunc,
        );
      });

      await RepoChargeHistory()
          .getList(page: _currentPage)
          .then((res) =>
      {
        BotToast.closeAllLoading(),
        setState(() {
          _totalItemCount = res.totalItemCount;
          _totalPage = res.totalPage;
          _list = [..._list, ...?res.list]; // 기존 리스트에 api에서 받아온 리스트 데이터를 더하는거.
          _currentPage++;
        })
      })
          .catchError((err) =>
      {
        BotToast.closeAllLoading(),
        print(err),
      });
    }

    // 새로고침하면 스크롤위치를 맨 위로 올리기.
    if (reFresh) {
      _scrollController.animateTo(
        0,
        duration: const Duration(milliseconds: 300),
        curve: Curves.easeOut,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.offset ==
          _scrollController.position.maxScrollExtent) {
        _loadItems();
      }
    });
    _loadItems();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: '나의 충전내역'),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _loadItems(reFresh: true);
        },
        child: const Icon(Icons.refresh),
      ),
      body: ListView(
        controller: _scrollController,
        children: [
          _buildBody(),
        ],
      ),
    );
  }

  _myChargeitemList(int index) {
    String content2Subject = "";
    String content2Text = "";
    switch ( _list[index].chargeType) {
      case "패스" :
        content2Subject = "충전 패스";
        content2Text = " 분";
        break;
      case "금액" :
        content2Subject = "충전 금액";
        content2Text = " 원";
        break;
      case "마일리지" :
        content2Subject = "충전 마일리지";
        content2Text = "";
        break;
    }
    return ComponentListTextLineItem(
      title: _list[index].dateReg,
      isUseContent1Line: true,
      content1Subject: "충전 타입",
      content1Text: _list[index].chargeType,
      isUseContent2Line: true,
      content2Subject: content2Subject,
      content2Text:
      maskWonFormatter.format(_list[index].charge) + content2Text,
      isUseContent3Line: true,
      content3Subject: '사용한 금액',
      content3Text: maskWonFormatter.format(_list[index].paymentAmount) + " 원",
      isUseContent4Line: true,
      content4Subject: '사용한 마일리지',
      content4Text: maskNumFormatter.format(_list[index].resultMileage),
      voidCallback: () {},
    );
  }


  Widget _buildBody() {
    if (_totalItemCount > 0) {
      return Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          ComponentCountTitle(Icons.attachment, _totalItemCount, '건', '충전내역'),
          ListView.builder(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              itemCount: _list.length,
              itemBuilder: (_, index) =>_myChargeitemList(index),
          )
        ],
      );
    } else {
      // 이용내역이 없을때 흰화면 보이면 에러난것처럼 보이니까 이용내역 없다고 보여주기.
      return SizedBox(
        height: MediaQuery
            .of(context)
            .size
            .height - 30 - 50 - 50 - 70,
        child: const ComponentNoContents(
          icon: Icons.history,
          msg: '이용내역이 없습니다.',
        ),
      );
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}
