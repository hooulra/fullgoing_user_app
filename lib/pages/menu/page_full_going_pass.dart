import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_custom_loading.dart';
import 'package:full_going_app/components/common/component_margin_horizon.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/components/component_box_full_going_pass.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/repository/repo_amount.dart';

class PageFullGoingPass extends StatefulWidget {
  const PageFullGoingPass({super.key});

  @override
  State<PageFullGoingPass> createState() => _PageFullGoingPassState();
}

class _PageFullGoingPassState extends State<PageFullGoingPass> {

  String oneHour = "MIN_60";
  String twoHour = "MIN_120";

  Future<void> _doChargeFullGoingPass({required String hourStr}) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoAmount().doChargeFullGoingPass(hourStr: hourStr).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '풀고잉 패스 충전 완료',
        subTitle: '풀고잉 패스 충전이 완료되었습니다.',
      ).call();

      Navigator.pop(context);

    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '풀고잉 패스 충전 실패',
        subTitle: '풀고잉 패스 충전이 실패했습니다.',
      ).call();
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  _ShowDialog(
      {required String title,
        required VoidCallback callbackBuy,
        required VoidCallback callbackCancel,
        String? content = ""}) {
    showDialog(
        context: context,
        //barrierDismissible - Dialog를 제외한 다른 화면 터치 x
        barrierDismissible: false,
        builder: (BuildContext context) {
          return AlertDialog(
            // RoundedRectangleBorder - Dialog 화면 모서리 둥글게 조절
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            //Dialog Main Title
            title: Text(title,
                style: TextStyle(
                    fontSize: fontSizeBig, fontWeight: FontWeight.bold)),
            actions: <Widget>[
              TextButton(
                child: Text("구매"),
                onPressed: callbackBuy,
              ),
              TextButton(
                child: Text("취소"),
                onPressed: callbackCancel,
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(
        title: '풀고잉 패스',
      ),
      body: SingleChildScrollView(
          child: Container(
            padding: bodyPaddingLeftRight,
            child: Column(
              children: [
                const ComponentMarginVertical(enumSize: EnumSize.big),
                Container(
                  width: MediaQuery.sizeOf(context).width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('1시간 동안, 자유롭게!',
                          style: TextStyle(
                              fontSize: fontSizeSuper,
                              fontWeight: FontWeight.bold)),
                      const ComponentMarginVertical(),
                      Text('최대 51% 할인',
                          style:
                          TextStyle(fontSize: fontSizeSuper, color: colorTeal)),
                      const ComponentMarginVertical(),
                      Container(
                        color: colorLightGray,
                        child: Text('이용 무제한 (패스 시간 한정)',
                            style: TextStyle(fontSize: fontSizeMid)),
                      ),
                    ],
                  ),
                ),
                const ComponentMarginVertical(enumSize: EnumSize.big),
                ComponentBoxFullGoingPassState(
                  title: "1시간 패스",
                  sub1Text: "60분 무제한 이용",
                  sub2Text: "~ 44%",
                  sub3Text: "4,900원",
                  sub4Text: "8,800",
                  voidCallback: () {
                    _ShowDialog(
                      title: "1시간 패스",
                      callbackBuy: () {
                        _doChargeFullGoingPass(hourStr: oneHour);
                        Navigator.pop(context);
                      },
                      callbackCancel: () {
                        Navigator.pop(context);
                      },
                    );
                  },
                ),
                ComponentBoxFullGoingPassState(
                  title: "2시간 패스",
                  sub1Text: "120분 무제한 이용",
                  sub2Text: "~ 51%",
                  sub3Text: "8,000원",
                  sub4Text: "16,600",
                  voidCallback: () {
                    _ShowDialog(
                      title: "2시간 패스",
                      callbackBuy: () {
                        _doChargeFullGoingPass(hourStr: twoHour);
                        Navigator.pop(context);
                      },
                      callbackCancel: () {
                        Navigator.pop(context);
                      },
                    );
                  },
                ),
              ],
            ),
          )),
    );
  }
}