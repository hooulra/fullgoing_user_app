
import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';


class PageKickBoardList extends StatefulWidget {
  const PageKickBoardList({super.key});

  @override
  State<PageKickBoardList> createState() => _PageKickBoardListState();
}

class _PageKickBoardListState extends State<PageKickBoardList> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "킥보드 리스트"),
    );
  }
}