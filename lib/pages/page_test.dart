import 'package:flutter/material.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';

class PageTest extends StatelessWidget {
  const PageTest({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(


      //body: _buildBody(),

    );
  }

  // Widget _buildBody() {
  //   return ListView(
  //     children: [
  //       Container(
  //         padding: const EdgeInsets.all(20),
  //         margin: const EdgeInsets.only(bottom: 10),
  //         child: Container(
  //           padding: const EdgeInsets.all(15),
  //           decoration: BoxDecoration(
  //             color: Colors.white,
  //             border: Border.all(color: colorLightGray),
  //             borderRadius: BorderRadius.circular(buttonRadius),
  //           ),
  //           child: Column(
  //             mainAxisSize: MainAxisSize.min,
  //             crossAxisAlignment: CrossAxisAlignment.stretch,
  //             children: [
  //               const Align(
  //                 alignment: Alignment.centerLeft,
  //                 child: Text(
  //                   "이용 내역",
  //                   style: TextStyle(
  //                     fontSize: fontSizeBig,
  //                     fontWeight: FontWeight.w700,
  //                   ),
  //                 ),
  //               ),
  //               const ComponentMarginVertical(),
  //               const Divider(height: 1),
  //               const ComponentMarginVertical(),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 children: [
  //                   const Text(
  //                     "킥보드 모델명",
  //                     style: TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                   Text(
  //                     kickBoardName,
  //                     style: const TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //               const ComponentMarginVertical(),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 children: [
  //                   const Text(
  //                     "킥보드 사용 시작 시간",
  //                     style: TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                   Text(
  //                     dateStart,
  //                     style: const TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //               const ComponentMarginVertical(),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 children: [
  //                   const Text(
  //                     "킥보드 사용 종료 시간",
  //                     style: TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                   Text(
  //                     dateEnd,
  //                     style: const TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //               const ComponentMarginVertical(enumSize: EnumSize.big),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 children: [
  //                   const Text(
  //                     "결제 금액",
  //                     style: TextStyle(
  //                         fontSize: fontSizeBig, fontWeight: FontWeight.w700),
  //                   ),
  //                   Text(
  //                     maskWonFormatter.format(resultPrice) + " 원",
  //                     style: const TextStyle(
  //                         fontSize: fontSizeBig, fontWeight: FontWeight.w700),
  //                   ),
  //                 ],
  //               ),
  //               const ComponentMarginVertical(),
  //               const Divider(height: 1),
  //               const ComponentMarginVertical(),
  //               Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //                 children: [
  //                   const Text(
  //                     "사용 패스 시간",
  //                     style: TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                   Text(
  //                     "${resultPass} 분",
  //                     style: const TextStyle(
  //                       fontSize: fontSizeMid,
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             ],
  //           ),
  //         ),
  //       ),
  //     ],
  //   );
  // }

  }

