import 'package:full_going_app/components/common/component_appbar_actions.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:intl/intl.dart';
import 'package:syncfusion_flutter_core/theme.dart';
import 'package:syncfusion_flutter_datagrid/datagrid.dart';

class PageStatistical extends StatefulWidget {
  const PageStatistical({super.key});

  @override
  State<PageStatistical> createState() => _PageStatisticalState();
}

class _PageStatisticalState extends State<PageStatistical> {
  final _formKey = GlobalKey<FormBuilderState>();
  final GlobalKey<SfDataGridState> _sfKey = GlobalKey<SfDataGridState>();

  List<Employee> employees = <Employee>[];
  late EmployeeDataSource employeeDataSource;


  @override
  void initState() {
    super.initState();
    employeeDataSource = EmployeeDataSource(employeeData: employees);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarPopup(title: "매출 통계"),
      body: Padding(
        padding: const EdgeInsets.only(right: 30, left: 30),
        child: FormBuilder(
          key: _formKey,
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            const SizedBox(
              height: 30,
            ),
            FormBuilderDateTimePicker(
              name: 'startDate',
              initialEntryMode: DatePickerEntryMode.input,
              inputType: InputType.date,
              lastDate: DateTime.now(),
              format: DateFormat('yyyy-MM-dd'),
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                labelText: '시작일',
              ),
            ),
            const SizedBox(height: 10,),
            FormBuilderDateTimePicker(
              name: 'endDate',
              initialEntryMode: DatePickerEntryMode.input,
              inputType: InputType.date,
              //firstDate: _formKey.currentState!.fields['startDate']!.value,
              lastDate: DateTime.now(),
              format: DateFormat('yyyy-MM-dd'),
              decoration: const InputDecoration(
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))),
                labelText: '종료일',
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Container(
              padding:
                  const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: FilledButton(
                style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all(Colors.blue.shade600),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                      RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  )),
                ),
                child: const Text(
                  "확인",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w600),
                ),
                onPressed: () {
                  setState(() {
                    employees = getEmployeeData();
                    employeeDataSource =
                        EmployeeDataSource(employeeData: employees);
                    print("확인");
                  });
                },
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(bottom: 50),
                child: SfDataGridTheme(
                  data: SfDataGridThemeData(
                    headerColor: Colors.grey.shade400,
                  ),
                  child: SfDataGrid(
                    key: _sfKey,
                    source: employeeDataSource,
                    columnWidthMode: ColumnWidthMode.fill,
                    tableSummaryRows: [
                      GridTableSummaryRow(
                          showSummaryInRow: true,
                          title: '매출 합계 : {Sum}원    매출 평균 : {Avg}원',
                          columns: [
                            const GridSummaryColumn(
                                name: 'Sum',
                                columnName: 'price',
                                summaryType: GridSummaryType.sum),
                            const GridSummaryColumn(
                              name: 'Avg',
                              columnName: 'price',
                              summaryType: GridSummaryType.average,
                            ),
                          ],
                          position: GridTableSummaryRowPosition.bottom)
                    ],
                    columns: <GridColumn>[
                      GridColumn(
                          columnWidthMode: ColumnWidthMode.fitByColumnName,
                          columnName: 'No',
                          label: Container(
                              padding: EdgeInsets.all(5.0),
                              alignment: Alignment.center,
                              child: const Text(
                                'No',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.w800),
                              ))),
                      GridColumn(
                        columnName: 'date',
                        label: Container(
                          padding: EdgeInsets.all(8.0),
                          alignment: Alignment.center,
                          child: const Text(
                            '날짜',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.w800),
                          ),
                        ),
                      ),
                      GridColumn(
                        columnName: 'price',
                        label: Container(
                          padding: EdgeInsets.all(8.0),
                          alignment: Alignment.center,
                          child: const Text(
                            '매출',
                            style: TextStyle(
                                fontSize: 15, fontWeight: FontWeight.w800),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  List<Employee> getEmployeeData() {
    return [
      Employee(1, '2020-01-01', 99999),
      Employee(2, '2020-01-02', 30000),
      Employee(3, '2020-01-03', 15000),
      Employee(4, '2020-01-04', 15000),
      Employee(5, '2020-01-05', 15000),
      Employee(6, '2020-01-06', 15000),
      Employee(7, '2020-01-07', 15000),
      Employee(8, '2020-01-08', 15000),
      Employee(9, '2020-01-09', 15000),
      Employee(10, '2020-01-10', 15000),
      Employee(11, '2020-01-10', 12348),
      Employee(12, '2020-01-10', 15000)
    ];
  }
}

/// Custom business object class which contains properties to hold the detailed
/// information about the employee which will be rendered in datagrid.
class Employee {
  /// Creates the employee class with required details.
  Employee(this.num, this.name, this.price);

  /// Id of an employee.
  final int num;

  /// Name of an employee.
  final String name;

  /// Salary of an employee.
  final int price;
}

/// An object to set the employee collection data source to the datagrid. This
/// is used to map the employee data to the datagrid widget.
class EmployeeDataSource extends DataGridSource {
  /// Creates the employee data source class with required details.
  EmployeeDataSource({required List<Employee> employeeData}) {
    _employeeData = employeeData
        .map<DataGridRow>((res) => DataGridRow(cells: [
              DataGridCell<int>(columnName: 'no', value: res.num),
              DataGridCell<String>(columnName: 'date', value: res.name),
              DataGridCell<int>(columnName: 'price', value: res.price),
            ]))
        .toList();
  }

  List<DataGridRow> _employeeData = [];

  @override
  List<DataGridRow> get rows => _employeeData;

  @override
  Widget? buildTableSummaryCellWidget(
      GridTableSummaryRow summaryRow,
      GridSummaryColumn? summaryColumn,
      RowColumnIndex rowColumnIndex,
      String summaryValue) {
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Text(summaryValue, style: TextStyle(fontWeight: FontWeight.w800)),
    );
  }

  @override
  DataGridRowAdapter buildRow(DataGridRow row) {
    late String retultStr;
    final formatCurrency = NumberFormat.currency(
      symbol: '₩',
      locale: "ko_KR",
      name: "",
      decimalDigits: 0,
    );

    return DataGridRowAdapter(
        cells: row.getCells().map<Widget>((res) {
      if (res.columnName == 'price') {
        retultStr = formatCurrency.format(res.value);
      } else {
        retultStr = res.value.toString();
      }

      return Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(8.0),
        child: Text(retultStr),
      );
    }).toList());
  }
}
