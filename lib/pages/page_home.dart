import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:full_going_app/components/common/component_appbar_actions.dart';
import 'package:full_going_app/components/common/component_custom_loading.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/config/config_api.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/functions/geo_check.dart';
import 'package:full_going_app/model/my_profile_status_item_result.dart';
import 'package:full_going_app/pages/menu/page_charge_amount.dart';
import 'package:full_going_app/pages/menu/page_full_going_pass.dart';
import 'package:full_going_app/pages/menu/page_how_to_use.dart';
import 'package:full_going_app/pages/menu/page_info.dart';
import 'package:full_going_app/pages/menu/page_my_charge_history.dart';
import 'package:full_going_app/pages/menu/page_my_use_history.dart';
import 'package:full_going_app/pages/page_qr_reader.dart';
import 'package:full_going_app/pages/profile/page_profile.dart';
import 'package:full_going_app/repository/repo_kickboard.dart';
import 'package:full_going_app/repository/repo_member.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';

import '../components/common/component_no_contents.dart';
import 'profile/page_license_reg.dart';

import 'dart:typed_data';
import 'package:image/image.dart' as IMG;

class PageHome extends StatefulWidget {
  const PageHome({super.key});

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  final Completer<GoogleMapController> _controller =
      Completer<GoogleMapController>();
  late GoogleMapController mapController;

  Position? _currentPosition;
  CameraPosition? _currentCamera;
  final _markers = <Marker>{};

  String _name = "";
  String _phoneNumber = "";
  num _remainMileage = 0;
  num _remainPass = 0;
  num _remainPrice = 0;
  bool _isLicence = false;

  String _directoryPath = "";
  String _defaultPath = "";
  String username = "";
  String phoneNumber = "";
  XFile? _pickedImage;
  Future<void> _initProfilePath() async {
    final Directory appDocumentsDir = await getApplicationDocumentsDirectory();
    _directoryPath = appDocumentsDir.path;
    _defaultPath = "$_directoryPath/profile.jpa";
    final File tempFile = File(_defaultPath);
    if (tempFile.existsSync()) {
      setState(() {
        print("불러오기");
        _pickedImage = XFile(_defaultPath);
      });
    }
  }

  void _startRoutine() async {
    await _getPosition().then((res) {
      _loadItems(res.latitude, res.longitude);
    });
  }

  void _getCurrentLocation() async {
    await _getPosition().then((Position position) async {
      setState(() {
        // Store the position in the variable
        _currentPosition = position;

        print('CURRENT POS: $_currentPosition');

        // For moving the camera to current location
        mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
              target: LatLng(position.latitude, position.longitude),
              zoom: 17.0,
            ),
          ),
        );
      });
    }).catchError((e) {
      print(e);
    });
  }

  Uint8List resizeImage(Uint8List data, width, height) {
    Uint8List? resizedData = data;
    IMG.Image? img = IMG.decodeImage(data);
    IMG.Image resized = IMG.copyResize(img!, width: width, height: height);
    resizedData = Uint8List.fromList(IMG.encodePng(resized));
    return resizedData;
  }

  Future<BitmapDescriptor> createCustomMarkerBitmap() async {
    ByteData data =
        await rootBundle.load('assets/img_kickboard_loading.png'); // 이미지 경로
    Uint8List bytes = data.buffer.asUint8List();
    return BitmapDescriptor.fromBytes(resizeImage(bytes, 90, 90));
  }

  Future<void> _loadItems(double posX, double posY) async {
    final markerIcon = await createCustomMarkerBitmap();
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(
        cancelFunc: cancelFunc,
      );
    });

    await RepoKickboard()
        .getList(posX, posY)
        .then((res) => {
              BotToast.closeAllLoading(),
              setState(() {
                this._markers.addAll(res.list!.map((e) => Marker(
                      icon: markerIcon,
                      onTap: () {
                        print("${e.modelName} 입니다.");
                      },
                      markerId: MarkerId(e.modelName),
                      infoWindow: InfoWindow(
                          title:
                              '[${e.kickBoardStatusName}] ${e.modelName} (${e.priceBasisName})'),
                      position: LatLng(e.posX as double, e.posY as double),
                    )));
              })
            })
        .catchError((err) => {
              BotToast.closeAllLoading(),
            });
  }

  Future<Position> _getPosition() async {
    try {
      Position position = await GeoCheck.determinePosition();

      setState(() {
        this._currentPosition = position;
        this._currentCamera = CameraPosition(
          target: LatLng(
            this._currentPosition!.latitude,
            this._currentPosition!.longitude,
          ),
          zoom: 17.0,
        );
      });

      return position;
    } catch (e) {
      ComponentNotification(
        success: false,
        title: '근처 킥보드 로딩 실패',
        subTitle: e.toString(),
      ).call();

      return Future.error('킥보드 로딩 실패');
    }
  }

  Future<void> _loadProfileData() async {
    var repository = RepoMember(); // 여기에 Repository 클래스 이름을 넣으세요.
    MyProfileStatusItemResult result = await repository.getList();

    setState(() {
      _name = result.data.name;
      _phoneNumber = result.data.phoneNumber;
      _remainMileage = result.data.remainMileage;
      _remainPass = result.data.remainPass;
      _remainPrice = result.data.remainPrice;
      _isLicence = result.data.isLicence;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadProfileData();
    _startRoutine();
    _initProfilePath();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const ComponentAppbarActions(title: " "),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: Stack(
        children: [
          Align(
            alignment: Alignment.bottomCenter,
            child: FloatingActionButton.large(
              heroTag: "btn1",
              child: Icon(Icons.qr_code_scanner_outlined),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => PageQrReader(
                          posX: _currentPosition!.latitude,
                          posY: _currentPosition!.longitude)),
                );
              },
            ),
          ),
          Container(
              alignment: Alignment.bottomRight,
              child: Padding(
                padding: EdgeInsets.only(right: 10, bottom: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FloatingActionButton.small(
                      heroTag: "btn2",
                      child: Icon(Icons.gps_fixed, color: Colors.white),
                      onPressed: () {
                        _getCurrentLocation();
                      },
                    ),
                  ],
                ),
              )),
        ],
      ),
      body: _buildBody(),
      drawer: Drawer(
        elevation: 1,
        backgroundColor: colorBackGround,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            GestureDetector(
              child: UserAccountsDrawerHeader(
                margin: EdgeInsets.zero,
                otherAccountsPicturesSize: Size(70, 70),
                otherAccountsPictures: [
                  _isLicence
                      ? Container(
                          child: Column(
                            children: [
                              Icon(
                                Icons.credit_card_rounded,
                                color: colorDarkGray,
                                size: 30,
                              ),
                              Text(
                                "등록 완료",
                                style: TextStyle(
                                    fontSize: fontSizeMicro,
                                    color: colorDarkGray),
                              )
                            ],
                          ),
                        )
                      : GestureDetector(
                          child: Container(
                            child: Column(
                              children: [
                                Icon(Icons.credit_card_off_outlined,
                                    size: 30, color: colorLightRed),
                                Text(
                                  "등록 필요",
                                  style: TextStyle(
                                      fontSize: fontSizeMicro,
                                      color: colorLightRed),
                                )
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (BuildContext context) =>
                                        PageLicenseReg())).then((value) {
                              setState(() {
                                _loadProfileData();
                              });
                            });
                          },
                        ),
                ],
                accountName: Text("$_name님 환영합니다.",
                    style: TextStyle(fontSize: fontSizeMid)),
                accountEmail: null,
                currentAccountPicture: _pickedImage != null
                    ? CircleAvatar(
                    backgroundImage: FileImage(File(_pickedImage!.path)))
                    : CircleAvatar(
                    backgroundImage:
                    AssetImage("assets/img_default_profile.png")),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                        const PageProfile())).then((value) {
                  setState(() {
                    _initProfilePath();
                    _loadProfileData();
                  });
                });
              },
            ),
            GestureDetector(
              child: Container(
                margin: EdgeInsets.zero,
                padding:
                    EdgeInsets.only(top: 5, bottom: 10, left: 10, right: 10),
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(10),
                        bottomRight: Radius.circular(10))),
                child: Padding(
                  padding: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                          "금액 : " +
                              maskWonFormatter.format(_remainPrice) +
                              " 원",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: fontSizeSm,
                          )),
                      Text("플고잉 패스 잔여 (분) : $_remainPass 분",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: fontSizeSm,
                          )),
                      Text("마일리지 : " + maskNumFormatter.format(_remainMileage),
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: fontSizeSm,
                          )),
                      ComponentMarginVertical()
                    ],
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => const PageProfile()),
                );
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.person,
                size: 40,
              ),
              title: const Text(
                '내 정보',
                style: TextStyle(fontSize: fontSizeBig),
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                        const PageProfile())).then((value) {
                  setState(() {
                    _initProfilePath();
                    _loadProfileData();
                  });
                });
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.electric_scooter_outlined,
                size: 40,
              ),
              title: const Text(
                '풀고잉 패스',
                style: TextStyle(fontSize: fontSizeBig),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            const PageFullGoingPass())).then((value) {
                  setState(() {
                    _loadProfileData();
                  });
                });
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.payment_outlined,
                size: 40,
              ),
              title: const Text(
                '금액충전',
                style: TextStyle(fontSize: fontSizeBig),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (BuildContext context) =>
                            const PageChargeAmount())).then((value) {
                  setState(() {
                    _loadProfileData();
                  });
                });
              },
            ),
            ListTile(
              leading: const Icon(
                //Icons.payments_outlined,
                Icons.receipt_outlined,
                size: 40,
              ),
              title: const Text(
                '충전내역',
                style: TextStyle(fontSize: fontSizeBig),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          const PageMyChargeHistory()),
                );
              },
            ),
            ListTile(
              leading: const Icon(
                //Icons.payments_outlined,
                Icons.library_books_rounded,
                size: 40,
              ),
              title: const Text(
                '이용내역',
                style: TextStyle(fontSize: fontSizeBig),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          const PageMyUseHistory()),
                );
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.menu_book,
                size: 40,
              ),
              title: const Text(
                '이용방법',
                style: TextStyle(fontSize: fontSizeBig),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => const PageHowToUse()),
                );
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.help_outline,
                size: 40,
              ),
              title: const Text(
                '고객지원',
                style: TextStyle(fontSize: fontSizeBig),
                textAlign: TextAlign.start,
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => const PageInfo()),
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBody() {
    if (_currentPosition != null) {
      return GoogleMap(
        mapType: MapType.normal,
        markers: _markers,
        initialCameraPosition: _currentCamera!,
        myLocationEnabled: true,
        myLocationButtonEnabled: false,
        zoomGesturesEnabled: true,
        zoomControlsEnabled: false,
        mapToolbarEnabled: false,
        onMapCreated: (GoogleMapController controller) {
          mapController = controller;
          _controller.complete(controller);
        },
      );
    } else {
      return SizedBox(
        height: MediaQuery.of(context).size.height - 30 - 50 - 50 - 70,
        child: const ComponentNoContents(
          icon: Icons.gps_fixed,
          msg: '위치정보가 있어야만 사용가능합니다.',
        ),
      );
    }
  }
}
