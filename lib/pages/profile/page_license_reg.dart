import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_custom_loading.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_form_validator.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/model/license_update_request.dart';
import 'package:full_going_app/repository/repo_member.dart';
import 'package:full_going_app/styles/style_form_decoration_full_going.dart';

class PageLicenseReg extends StatefulWidget {
  PageLicenseReg({super.key});

  @override
  State<PageLicenseReg> createState() => _PageLicenseRegState();
}

class _PageLicenseRegState extends State<PageLicenseReg> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLicenseUpdate(
      LicenseUpdateRequest licenseUpdateRequest) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doLicence(licenseUpdateRequest).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '면허증 등록 완료',
        subTitle: '면허증 등록이 완료 되었습니다.',
      ).call();

      Navigator.pop(context);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '면허증 등록 실패',
        subTitle: '면허증 등록이 실패 하였습니다. 다시 시도해주세요',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(
        title: "면허 등록",
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: bodyPaddingLeftRight,
          child: FormBuilder(
            key: _formKey,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              const ComponentMarginVertical(enumSize: EnumSize.big),
              Image.asset(
                "assets/img_license_sample.jpg",
                fit: BoxFit.cover,
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                Icon(Icons.circle, size: 10),
                SizedBox(width: 5),
                Container(
                  width: MediaQuery.sizeOf(context).width - 100,
                  child: FittedBox(
                      fit: BoxFit.contain,
                      child: Text("위 이미지를 참고하시고 해당 정보를 입력해주세요.",
                          style: TextStyle(fontSize: fontSizeSm))),
                ),
              ]),
              SizedBox(height: 20),
              FormBuilderTextField(
                name: 'licenceNumber',
                keyboardType: TextInputType.number,
                inputFormatters: [
                  maskLicenseFormatter,
                  //13자리만 입력받도록 하이픈 3개+숫자 12개
                ],
                decoration: StyleFormDecorationOfFullGoing().getInputDecoration(
                    "운전면허번호",
                    useHintText: true,
                    hintText: "XX-XX-XXXXXX-XX"),
                validator: FormBuilderValidators.compose([
                  FormBuilderValidators.required(errorText: formErrorRequired),
                  FormBuilderValidators.minLength(15,
                      errorText: formErrorMinLength(15)),
                ]),
              ),
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ComponentTextBtn(
                  text: "등록",
                  textColor: Colors.white,
                  borderColor: colorPrimary,
                  callback: () {
                    if (_formKey.currentState!.saveAndValidate()) {
                      LicenseUpdateRequest licenseUpdateRequest =
                          LicenseUpdateRequest(
                        _formKey.currentState!.fields['licenceNumber']!.value,
                      );
                      _doLicenseUpdate(licenseUpdateRequest);
                    }
                  },
                ),
              ),
            ]),
          ),
        ),
      ),
    );
  }
}
