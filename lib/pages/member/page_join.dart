import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:full_going_app/components/common/component_appbar_popup.dart';
import 'package:full_going_app/components/common/component_custom_loading.dart';
import 'package:full_going_app/components/common/component_margin_vertical.dart';
import 'package:full_going_app/components/common/component_notification.dart';
import 'package:full_going_app/components/common/component_text_btn.dart';
import 'package:full_going_app/config/config_color.dart';
import 'package:full_going_app/config/config_form_formatter.dart';
import 'package:full_going_app/config/config_form_validator.dart';
import 'package:full_going_app/config/config_size.dart';
import 'package:full_going_app/config/config_style.dart';
import 'package:full_going_app/enums/enum_size.dart';
import 'package:full_going_app/model/auth_check_request.dart';
import 'package:full_going_app/model/auth_send_request.dart';
import 'package:full_going_app/model/member_join_request.dart';
import 'package:full_going_app/repository/repo_member.dart';
import 'package:full_going_app/repository/repo_phone_auth.dart';
import 'package:full_going_app/styles/style_form_decoration_full_going.dart';

import 'page_login.dart';

class PageJoin extends StatefulWidget {
  const PageJoin({super.key});

  @override
  State<PageJoin> createState() => _PageJoinState();
}

class _PageJoinState extends State<PageJoin> {
  final _formKey = GlobalKey<FormBuilderState>();

  bool _isPasswordVisible = true;
  bool _isRePasswordVisible = true;

  String _authSendBtnName = '인증요청';
  bool _isAuthNumSend = false;
  bool _isAuthNumSuccess = false;
  final int _maxAuthNumLength = 6;
  String? _textAuthNum = "";

  Future<void> _doAuthSend(AuthSendRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doSend(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 전송',
        subTitle: '인증번호가 ${request.phoneNumber} 으로 전송되었습니다.',
      ).call();

      setState(() {
        _authSendBtnName = '인증번호 재전송';
        _isAuthNumSend = res.isSuccess;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();
      ComponentNotification(
        success: false,
        title: '인증번호 전송 실패',
        subTitle: '고객센터로 문의하세요.',
      ).call();
    });
  }

  Future<void> _doAuthCheck(AuthCheckRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoPhoneAuth().doCheck(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '인증번호 확인 완료',
        subTitle: '인증번호 확인이 완료되었습니다. 회원가입버튼을 눌러 가입을 완료해주세요.',
      ).call();

      setState(() {
        _isAuthNumSuccess = res.isSuccess;
      });
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '인증번호 확인 실패',
        subTitle: '인증번호를 정확히 입력해주세요.',
      ).call();
    });
  }

  Future<void> _doJoin(MemberJoinRequest request) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoMember().doJoin(request).then((res) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: true,
        title: '회원가입 완료',
        subTitle: '회원가입이 완료되었습니다. 로그인을 해주세요.',
      ).call();

      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageLogin()),
          (route) => false);
    }).catchError((err) {
      BotToast.closeAllLoading();

      ComponentNotification(
        success: false,
        title: '회원가입 실패',
        subTitle: '입력값을 확인해주세요.',
      ).call();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: ComponentAppbarPopup(title: "회원가입"),
      body: SingleChildScrollView(
        child: Container(
          padding: bodyPaddingLeftRight,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const ComponentMarginVertical(enumSize: EnumSize.big),
              Container(
                alignment: Alignment.centerLeft,
                child: Text("FullGoing 회원가입.",
                    style: TextStyle(fontSize: fontSizeSuper)),
              ), // 회원가입 문구
              Container(
                alignment: Alignment.centerLeft,
                child: Text("휴대전화 인증 후 가입을 진행부탁드립니다.",
                    style: TextStyle(fontSize: fontSizeBig)),
              ), // 회원가입 서브 문구
              const ComponentMarginVertical(enumSize: EnumSize.mid),
              FormBuilder(
                key: _formKey,
                child: Column(
                  children: [
                    FormBuilderTextField(
                      keyboardType: TextInputType.text,
                      name: 'username',
                      inputFormatters: [maskNoInputKrFormatter],
                      maxLength: 20,
                      decoration: StyleFormDecorationOfFullGoing()
                          .getInputDecoration("아이디"),
                      validator: validatorOfLoginUsername,
                    ),
                    const ComponentMarginVertical(),
                    FormBuilderTextField(
                      name: 'password',
                      inputFormatters: [maskNoInputKrFormatter],
                      maxLength: 20,
                      obscureText: _isPasswordVisible,
                      decoration: StyleFormDecorationOfFullGoing()
                          .getInputDecorationOfPassword(
                        "비밀번호",
                        GestureDetector(
                          child: _isPasswordVisible
                              ? Icon(Icons.visibility_off,
                                  size: sizeSuffixIconOfPassword,
                                  color: colorPrimary)
                              : Icon(Icons.visibility,
                                  size: sizeSuffixIconOfPassword,
                                  color: colorPrimary),
                          onTap: () {
                            setState(() {
                              _isPasswordVisible = !_isPasswordVisible;
                            });
                          },
                        ),
                      ),
                      validator: validatorOfLoginPassword,
                    ), //password
                    const ComponentMarginVertical(),
                    FormBuilderTextField(
                      name: 'passwordRe',
                      inputFormatters: [maskNoInputKrFormatter],
                      maxLength: 20,
                      obscureText: _isRePasswordVisible,
                      decoration: StyleFormDecorationOfFullGoing()
                          .getInputDecorationOfPassword(
                        "비밀번호 확인",
                        GestureDetector(
                          child: _isRePasswordVisible
                              ? Icon(Icons.visibility_off,
                                  size: sizeSuffixIconOfPassword,
                                  color: colorPrimary)
                              : Icon(Icons.visibility,
                                  size: sizeSuffixIconOfPassword,
                                  color: colorPrimary),
                          onTap: () {
                            setState(() {
                              _isRePasswordVisible = !_isRePasswordVisible;
                            });
                          },
                        ),
                      ),
                      validator: (value) =>
                          _formKey.currentState?.fields['password']?.value !=
                                  value
                              ? formErrorEqualPassword
                              : null,
                    ), //rePassword
                    const ComponentMarginVertical(),
                    FormBuilderTextField(
                      name: 'name',
                      maxLength: 20,
                      decoration: StyleFormDecorationOfFullGoing()
                          .getInputDecoration("이름"),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(
                            errorText: formErrorRequired),
                        FormBuilderValidators.minLength(2,
                            errorText: formErrorMinLength(2)),
                        FormBuilderValidators.maxLength(20,
                            errorText: formErrorMaxLength(20)),
                      ]),
                    ), //memberName
                    const ComponentMarginVertical(),
                    FormBuilderTextField(
                      name: 'phoneNumber',
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        maskPhoneNumberFormatter
                        //13자리만 입력받도록 하이픈 2개+숫자 11개
                      ],
                      decoration:
                          StyleFormDecorationOfFullGoing().getInputDecoration(
                        "휴대전화",
                        useHintText: true,
                        hintText: "XXX-XXXX-XXXX",
                      ),
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(
                            errorText: formErrorRequired),
                        FormBuilderValidators.minLength(13,
                            errorText: formErrorMinLength(13)),
                        FormBuilderValidators.maxLength(13,
                            errorText: formErrorMaxLength(13)),
                      ]),
                      enabled: !_isAuthNumSuccess,
                    ),
                    const ComponentMarginVertical(),
                    !_isAuthNumSuccess
                        ? Container(
                            width: MediaQuery.sizeOf(context).width,
                            child: ComponentTextBtn(
                              text: '$_authSendBtnName',
                              bgColor: colorSecondary,
                              borderColor: colorSecondary,
                              callback: () {
                                String phoneNumberText = _formKey
                                    .currentState!.fields['phoneNumber']!.value;

                                if (phoneNumberText.length == 13) {
                                  AuthSendRequest authSendRequest =
                                      AuthSendRequest(phoneNumberText);
                                  _doAuthSend(authSendRequest);
                                  print(phoneNumberText);
                                }
                              },
                            ),
                          ) // 인증요청 버튼
                        : Container(),
                    //phoneNumber
                    const ComponentMarginVertical(),
                    _isAuthNumSend && !_isAuthNumSuccess
                        ? Container(
                            child: FormBuilderTextField(
                              name: 'authNum',
                              keyboardType: TextInputType.number,
                              maxLength: _maxAuthNumLength,
                              decoration: StyleFormDecorationOfFullGoing()
                                  .getInputDecoration("인증번호",
                                      useSuffixText: true,
                                      suffixText:
                                          "${_textAuthNum?.length} / $_maxAuthNumLength"),
                              onChanged: (value) {
                                setState(() {
                                  _textAuthNum = value;
                                });
                              },
                            ),
                          ) //phoneAuth
                        : Container(),
                  ],
                ),
              ),
              _isAuthNumSend && !_isAuthNumSuccess
                  ? Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      width: MediaQuery.sizeOf(context).width,
                      child: ComponentTextBtn(
                        text: '인증번호 확인',
                        bgColor: colorSecondary,
                        borderColor: colorSecondary,
                        callback: () {
                          String phoneNumberText = _formKey
                              .currentState!.fields['phoneNumber']!.value;
                          String authNumberText =
                              _formKey.currentState!.fields['authNum']!.value;

                          if (phoneNumberText.length == 13 &&
                              authNumberText.length == _maxAuthNumLength) {
                            AuthCheckRequest authCheckRequest =
                                AuthCheckRequest(
                              phoneNumberText,
                              authNumberText,
                            );
                            _doAuthCheck(authCheckRequest);
                          }
                        },
                      ),
                    )
                  : Container(),
              Container(
                width: MediaQuery.of(context).size.width,
                child: ComponentTextBtn(
                    text: '회원가입',
                    callback: () {
                      if (_formKey.currentState!.saveAndValidate()) {
                        MemberJoinRequest request = MemberJoinRequest(
                          _formKey.currentState!.fields['username']!.value,
                          _formKey.currentState!.fields['password']!.value,
                          _formKey.currentState!.fields['passwordRe']!.value,
                          _formKey.currentState!.fields['name']!.value,
                          _formKey.currentState!.fields['phoneNumber']!.value,
                        );
                        print('안녕');
                        _doJoin(request);
                      }
                    }),
              ),
              //회원가입 버튼
            ],
          ),
        ),
      ),
    );
  }
}
