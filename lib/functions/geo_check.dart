import 'package:geolocator/geolocator.dart';

class GeoCheck {
  static Future<Position> determinePosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return Future.error('GPS 기능이 꺼져있습니다.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('GPS 권한을 허용해야만 사용 가능 합니다.');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      // 앱에서 위치 권한 영구적으로 거부해버린거.. 설정가서 풀어줘야함..
      return Future.error(
          'GPS 권한이 차단되었습니다. 설정 > 앱에서 권한허용을 수기로 해주세요.');
    }

    return await Geolocator.getCurrentPosition();
  }
}