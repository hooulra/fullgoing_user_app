
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

var maskLicenseFormatter = MaskTextInputFormatter(
    mask: 'S#-##-######-##',
    filter: { "#": RegExp(r'[0-9]') , "S":  RegExp(r'[1-2]')},
    type: MaskAutoCompletionType.lazy
);

var maskPhoneNumberFormatter = MaskTextInputFormatter(
    mask: '###-####-####',
    filter: { "#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy
);


var maskAuthNumberFormatter = MaskTextInputFormatter(
    mask: '######',
    filter: { "#": RegExp(r'[0-9]')},
    type: MaskAutoCompletionType.lazy
);



var maskWonFormatter = NumberFormat.currency(locale: "ko_KR", symbol: "");
var maskNumFormatter = NumberFormat('###,###,###,###');
var maskNoInputKrFormatter = FilteringTextInputFormatter.allow(RegExp('[0-9a-zA-Z]'));







