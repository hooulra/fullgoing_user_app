import 'package:flutter/material.dart';
import 'package:full_going_app/config/config_color.dart';

const double buttonRadius = 10;
const double buttonSideWidth = 2;

const double buttonElevation = 0;
const double appBarElevation = 1;

const EdgeInsets bodyPaddingNone = EdgeInsets.only();
const EdgeInsets bodyPaddingAll = EdgeInsets.all(10);
const EdgeInsets bodyPaddingLeftRightAndVerticalHalf =
    EdgeInsets.only(left: 20, right: 20, top: 10, bottom: 10);
const EdgeInsets bodyPaddingLeftRight = EdgeInsets.only(
  left: 20,
  right: 20,
);
const EdgeInsets filterPaddingLeftRight = EdgeInsets.only(
  left: 0,
  right: 0,
);
const EdgeInsets contentPaddingButton = EdgeInsets.only(
  top: 15,
  bottom: 15,
);

const EdgeInsets contentPaddingSmButton = EdgeInsets.only(
  top: 5,
  bottom: 5,
);

const EdgeInsets contentPaddingAllButton = EdgeInsets.all(5);



const EdgeInsets contentPaddingLeftRightOfFullGoingPassBox = EdgeInsets.only(
  left: 20,
  right: 20,
  top: 10,
  bottom: 10,
);


const EdgeInsets contentMarginLeftRightOfFullGoingPassBox = EdgeInsets.only(
  bottom: 20,
);



BoxDecoration formBoxDecoration = BoxDecoration(
  color: Colors.white,
  border: Border.all(color: colorLightGray),
);

const BoxDecoration filterBoxDecoration = BoxDecoration(
  color: Colors.white,
  border: Border(
    top: BorderSide(width: 1, color: colorLightGray),
  ),
);
